package com.example.demo.service.impl;

import com.example.demo.controller.CountryParams;
import com.example.demo.dao.CountriesRepository;
import com.example.demo.dao.model.CountryEntity;
import com.example.demo.dto.CountryDto;
import com.example.demo.service.CountriesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CountriesServiceImpl implements CountriesService {

    private final CountriesRepository repository;

    public List<CountryDto> getCountriesByParam(CountryParams params) {
        log.debug("getCountriesByParam(CountryParams params)::params = {}", params);

        final List<CountryDto> result = new ArrayList<>();

        if (params.getId() != null) {
            repository.findById(params.getId()).ifPresent(country -> result.add(CountryDto.builder()
                    .id(country.getId())
                    .name(country.getName())
                    .build()));
        } else if (StringUtils.isNotBlank(params.getName())) {
            repository.findByName(params.getName()).ifPresent(country -> result.add(CountryDto.builder()
                    .id(country.getId())
                    .name(country.getName())
                    .build()));
        } else {
            // TODO: Add Page<CountryDto> findAll(Pageable pageable);
            List<CountryEntity> found = repository.findAll();
            result.addAll(found.stream().map(item -> CountryDto.builder()
                    .id(item.getId())
                    .name(item.getName())
                    .build()).collect(Collectors.toList()));
        }

        log.debug("Result of getCountriesByParam(CountryParams params)::result = {}", result);
        return result;
    }

    public CountryDto createCountry(CountryDto country) {
        log.debug("createCountry(CountryDto country)::country = {}", country);

        CountryEntity countryEntity = CountryEntity.builder()
                .name(country.getName())
                .build();

        CountryEntity saved = repository.save(countryEntity);

        country.setId(saved.getId());
        log.debug("Result of createCountry(CountryDto country)::country = {}", country);

        return country;
    }
}
