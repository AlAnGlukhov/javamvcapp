package com.example.demo.service;

import com.example.demo.controller.CountryParams;
import com.example.demo.dto.CountryDto;

import java.util.List;

public interface CountriesService {
    List<CountryDto> getCountriesByParam(CountryParams params);
    CountryDto createCountry(CountryDto country);
}
