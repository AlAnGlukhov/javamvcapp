package com.example.demo.controller.impl;

import com.example.demo.controller.CountriesOperations;
import com.example.demo.controller.CountryParams;
import com.example.demo.dto.CountryDto;
import com.example.demo.service.CountriesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CountriesController implements CountriesOperations {

    private final CountriesService service;

    @Override
    public List<CountryDto> getCountriesByParam(Long id, String name, String text, Integer offset, Integer limit) {
        log.debug("getReposByParam(String id, String name, String text, Integer offset, Integer limit)::" +
                "id = {}, name = {}, text = {}, offset = {}, limit = {}", id, name, text, offset, limit);

        CountryParams params = CountryParams.builder()
                .id(id)
                .name(name)
                .text(text)
                .limit(limit)
                .offset(offset)
                .build();

        List<CountryDto> result = service.getCountriesByParam(params);
        log.debug("Result of getReposByParam: {}", result);

        return result;
    }

    @Override
    public CountryDto createCountry(CountryDto repo) {
        log.debug("createRepo(RepoDto repo)::repo = {}", repo);

        CountryDto result = service.createCountry(repo);
        log.debug("Result of createRepo: {}", result);

        return result;
    }
}
