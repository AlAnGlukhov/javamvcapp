package com.example.demo.controller;

import com.example.demo.dto.CountryDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Validated
@Tag(name = "Справочник ...", description = "Хранилище ...")
public interface CountriesOperations {

    @GetMapping("/countries")
    List<CountryDto> getCountriesByParam(@RequestParam(value = "id", required = false) Long id,
                                         @RequestParam(value = "name", required = false) String name,
                                         @RequestParam(value = "text", required = false) String text,
                                         @RequestParam(value = "offset", required = false) Integer offset,
                                         @RequestParam(value = "limit", required = false) Integer limit);

    @PostMapping("/countries")
    CountryDto createCountry(@Valid @RequestBody CountryDto repo);
}
