package com.example.demo.controller.impl;

import com.example.demo.dto.CountryDto;
import com.example.demo.service.CountriesService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@WebMvcTest(CountriesController.class)
class CountriesControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CountriesService service;

    @Test
    @SneakyThrows
    void getCountriesByParamTest() {
        // Given
        Long id = 123L;
        List<CountryDto> result = Collections.singletonList(CountryDto.builder()
                .id(id)
                .build());

        // When
        when(service.getCountriesByParam(any())).thenReturn(result);

        // Then
        mvc.perform(get("/countries/")
                        .param("id", String.valueOf(id))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id").value(id));
    }

    @Test
    @SneakyThrows
    void createCountryTest() {
        // Given
        String countryName = "TEST_NAME";
        CountryDto request = CountryDto.builder()
                .name(countryName)
                .build();

        Long id = 11L;
        CountryDto result = CountryDto.builder()
                .id(id)
                .name(request.getName())
                .build();

        // When
        when(service.createCountry(any(CountryDto.class))).thenReturn(result);

        // Then
        mvc.perform(post("/countries/")
                        .content(toJson(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.name").value(countryName));
    }

    private String toJson(final CountryDto country) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(country);
    }
}